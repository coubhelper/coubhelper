﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace techsupport
{
    public partial class Profile : Form
    {
        public Profile()
        {
            InitializeComponent();
        }

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Личный кабинет",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private Form changePassword = new ChangePassword();

        private async void saveButton_Click(object sender, EventArgs e)
        {
            if (textBoxFirstName.Text == "" || textBoxLastName.Text == "" || textBoxMiddleName.Text == "" || textBoxCountry.Text == "" || textBoxCity.Text == "")
            {
                errorBox("Все поля должны быть заполнены");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("UPDATE accounts SET lastName = @lastname, firstName = @firstname, middleName = @middleName, country = @country, city = @city WHERE id = @id", DB);
                command.Parameters.AddWithValue("@lastname", textBoxLastName.Text);
                command.Parameters.AddWithValue("@firstname", textBoxFirstName.Text);
                command.Parameters.AddWithValue("@middleName", textBoxMiddleName.Text);
                command.Parameters.AddWithValue("@country", textBoxCountry.Text);
                command.Parameters.AddWithValue("@city", textBoxCity.Text);
                command.Parameters.AddWithValue("@id", Session.id);

                // Обновление данных в сессии
                Session.lastName = textBoxLastName.Text;
                Session.firstName = textBoxFirstName.Text;
                Session.middleName = textBoxMiddleName.Text;
                Session.country = textBoxCountry.Text;
                Session.city = textBoxCity.Text;

                await command.ExecuteNonQueryAsync();

                MessageBox.Show("Изменения внесены успешно");
            }
        }

        private SQLiteConnection DB;

        private void changePassword_Click(object sender, EventArgs e)
        {
            if (changePassword.IsDisposed)
                changePassword = new ChangePassword();
            changePassword.Show();
            changePassword.Focus();
        }

        private async void Profile_Load(object sender, EventArgs e)
        {
            textBoxFirstName.Text = Session.firstName;
            textBoxLastName.Text = Session.lastName;
            textBoxMiddleName.Text = Session.middleName;
            textBoxMail.Text = Session.email;
            textBoxCountry.Text = Session.country;
            textBoxCity.Text = Session.city;
            textBoxNumber.Text = Session.phone;

            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        private void Profile_FormClosing(object sender, FormClosingEventArgs e)
        {
            changePassword.Close();
        }
    }
}
 