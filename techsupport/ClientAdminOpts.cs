﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace techsupport
{
    public partial class ClientAdminOpts : Form
    {
        public string currentId;
        public SQLiteConnection DB;
        public ClientAdminOpts(string currentId)
        {
            InitializeComponent();
            this.currentId = currentId;
        }

        private async void ButtonRemove_Click(object sender, EventArgs e)
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM accounts WHERE id = @id", DB);
            cmd.Parameters.AddWithValue("@id", currentId);
            await cmd.ExecuteNonQueryAsync();
            MessageBox.Show("Пользователь удален");
            this.Close();
        }

        private async void ClientAdminOpts_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }
    }
}
