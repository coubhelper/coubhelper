﻿
namespace techsupport
{
    partial class AddStaff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddStaff));
            this.label_AddStaff = new System.Windows.Forms.Label();
            this.textBoxFIO = new System.Windows.Forms.TextBox();
            this.textBoxMail = new System.Windows.Forms.TextBox();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.textBoxPassAgain = new System.Windows.Forms.TextBox();
            this.label_FIO = new System.Windows.Forms.Label();
            this.label_Mail = new System.Windows.Forms.Label();
            this.label_Country = new System.Windows.Forms.Label();
            this.label_City = new System.Windows.Forms.Label();
            this.label_Number = new System.Windows.Forms.Label();
            this.label_Password = new System.Windows.Forms.Label();
            this.label_PasswordAgain = new System.Windows.Forms.Label();
            this.button_AddStaff = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_AddStaff
            // 
            this.label_AddStaff.AutoSize = true;
            this.label_AddStaff.BackColor = System.Drawing.Color.Black;
            this.label_AddStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_AddStaff.ForeColor = System.Drawing.SystemColors.Control;
            this.label_AddStaff.Location = new System.Drawing.Point(13, 13);
            this.label_AddStaff.Name = "label_AddStaff";
            this.label_AddStaff.Size = new System.Drawing.Size(229, 25);
            this.label_AddStaff.TabIndex = 0;
            this.label_AddStaff.Text = "Добавить сотрудника";
            // 
            // textBoxFIO
            // 
            this.textBoxFIO.Location = new System.Drawing.Point(18, 62);
            this.textBoxFIO.Multiline = true;
            this.textBoxFIO.Name = "textBoxFIO";
            this.textBoxFIO.Size = new System.Drawing.Size(224, 24);
            this.textBoxFIO.TabIndex = 1;
            // 
            // textBoxMail
            // 
            this.textBoxMail.Location = new System.Drawing.Point(18, 104);
            this.textBoxMail.Multiline = true;
            this.textBoxMail.Name = "textBoxMail";
            this.textBoxMail.Size = new System.Drawing.Size(224, 24);
            this.textBoxMail.TabIndex = 2;
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Location = new System.Drawing.Point(18, 148);
            this.textBoxCountry.Multiline = true;
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(224, 24);
            this.textBoxCountry.TabIndex = 3;
            // 
            // textBoxCity
            // 
            this.textBoxCity.Location = new System.Drawing.Point(18, 195);
            this.textBoxCity.Multiline = true;
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(224, 24);
            this.textBoxCity.TabIndex = 4;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Location = new System.Drawing.Point(18, 239);
            this.textBoxPhone.Multiline = true;
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(224, 24);
            this.textBoxPhone.TabIndex = 5;
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(18, 286);
            this.textBoxPass.Multiline = true;
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(224, 24);
            this.textBoxPass.TabIndex = 6;
            // 
            // textBoxPassAgain
            // 
            this.textBoxPassAgain.Location = new System.Drawing.Point(18, 333);
            this.textBoxPassAgain.Multiline = true;
            this.textBoxPassAgain.Name = "textBoxPassAgain";
            this.textBoxPassAgain.Size = new System.Drawing.Size(224, 24);
            this.textBoxPassAgain.TabIndex = 7;
            // 
            // label_FIO
            // 
            this.label_FIO.AutoSize = true;
            this.label_FIO.BackColor = System.Drawing.Color.Black;
            this.label_FIO.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label_FIO.ForeColor = System.Drawing.Color.White;
            this.label_FIO.Location = new System.Drawing.Point(15, 46);
            this.label_FIO.Name = "label_FIO";
            this.label_FIO.Size = new System.Drawing.Size(34, 13);
            this.label_FIO.TabIndex = 8;
            this.label_FIO.Text = "ФИО";
            // 
            // label_Mail
            // 
            this.label_Mail.AutoSize = true;
            this.label_Mail.BackColor = System.Drawing.Color.Black;
            this.label_Mail.ForeColor = System.Drawing.Color.White;
            this.label_Mail.Location = new System.Drawing.Point(15, 89);
            this.label_Mail.Name = "label_Mail";
            this.label_Mail.Size = new System.Drawing.Size(35, 13);
            this.label_Mail.TabIndex = 9;
            this.label_Mail.Text = "E-mail";
            // 
            // label_Country
            // 
            this.label_Country.AutoSize = true;
            this.label_Country.BackColor = System.Drawing.Color.Black;
            this.label_Country.ForeColor = System.Drawing.Color.White;
            this.label_Country.Location = new System.Drawing.Point(15, 132);
            this.label_Country.Name = "label_Country";
            this.label_Country.Size = new System.Drawing.Size(43, 13);
            this.label_Country.TabIndex = 10;
            this.label_Country.Text = "Страна";
            // 
            // label_City
            // 
            this.label_City.AutoSize = true;
            this.label_City.BackColor = System.Drawing.Color.Black;
            this.label_City.ForeColor = System.Drawing.Color.White;
            this.label_City.Location = new System.Drawing.Point(16, 179);
            this.label_City.Name = "label_City";
            this.label_City.Size = new System.Drawing.Size(37, 13);
            this.label_City.TabIndex = 11;
            this.label_City.Text = "Город";
            // 
            // label_Number
            // 
            this.label_Number.AutoSize = true;
            this.label_Number.BackColor = System.Drawing.Color.Black;
            this.label_Number.ForeColor = System.Drawing.Color.White;
            this.label_Number.Location = new System.Drawing.Point(16, 223);
            this.label_Number.Name = "label_Number";
            this.label_Number.Size = new System.Drawing.Size(93, 13);
            this.label_Number.TabIndex = 12;
            this.label_Number.Text = "Номер телефона";
            // 
            // label_Password
            // 
            this.label_Password.AutoSize = true;
            this.label_Password.BackColor = System.Drawing.Color.Black;
            this.label_Password.ForeColor = System.Drawing.Color.White;
            this.label_Password.Location = new System.Drawing.Point(16, 270);
            this.label_Password.Name = "label_Password";
            this.label_Password.Size = new System.Drawing.Size(45, 13);
            this.label_Password.TabIndex = 13;
            this.label_Password.Text = "Пароль";
            // 
            // label_PasswordAgain
            // 
            this.label_PasswordAgain.AutoSize = true;
            this.label_PasswordAgain.BackColor = System.Drawing.Color.Black;
            this.label_PasswordAgain.ForeColor = System.Drawing.Color.White;
            this.label_PasswordAgain.Location = new System.Drawing.Point(16, 317);
            this.label_PasswordAgain.Name = "label_PasswordAgain";
            this.label_PasswordAgain.Size = new System.Drawing.Size(127, 13);
            this.label_PasswordAgain.TabIndex = 14;
            this.label_PasswordAgain.Text = "Подтверждение пароля";
            // 
            // button_AddStaff
            // 
            this.button_AddStaff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_AddStaff.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_AddStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AddStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_AddStaff.ForeColor = System.Drawing.Color.White;
            this.button_AddStaff.Location = new System.Drawing.Point(12, 377);
            this.button_AddStaff.Name = "button_AddStaff";
            this.button_AddStaff.Size = new System.Drawing.Size(272, 34);
            this.button_AddStaff.TabIndex = 15;
            this.button_AddStaff.Text = "Добавить сотрудника";
            this.button_AddStaff.UseVisualStyleBackColor = false;
            this.button_AddStaff.Click += new System.EventHandler(this.button_AddStaff_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-4, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(725, 456);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // AddStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 450);
            this.Controls.Add(this.button_AddStaff);
            this.Controls.Add(this.label_PasswordAgain);
            this.Controls.Add(this.label_Password);
            this.Controls.Add(this.label_Number);
            this.Controls.Add(this.label_City);
            this.Controls.Add(this.label_Country);
            this.Controls.Add(this.label_Mail);
            this.Controls.Add(this.label_FIO);
            this.Controls.Add(this.textBoxPassAgain);
            this.Controls.Add(this.textBoxPass);
            this.Controls.Add(this.textBoxPhone);
            this.Controls.Add(this.textBoxCity);
            this.Controls.Add(this.textBoxCountry);
            this.Controls.Add(this.textBoxMail);
            this.Controls.Add(this.textBoxFIO);
            this.Controls.Add(this.label_AddStaff);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AddStaff";
            this.Text = "Добавить сотрудника";
            this.Load += new System.EventHandler(this.AddStaff_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_AddStaff;
        private System.Windows.Forms.TextBox textBoxFIO;
        private System.Windows.Forms.TextBox textBoxMail;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.TextBox textBoxPassAgain;
        private System.Windows.Forms.Label label_FIO;
        private System.Windows.Forms.Label label_Mail;
        private System.Windows.Forms.Label label_Country;
        private System.Windows.Forms.Label label_City;
        private System.Windows.Forms.Label label_Number;
        private System.Windows.Forms.Label label_Password;
        private System.Windows.Forms.Label label_PasswordAgain;
        private System.Windows.Forms.Button button_AddStaff;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}