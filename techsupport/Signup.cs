﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace techsupport
{
    public partial class Signup : Form
    {
        public Signup()
        {
            InitializeComponent();
        }

        private SQLiteConnection DB;

        private async void Signup_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Регистрация",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private async void signupSubmit_Click(object sender, EventArgs e)
        {
            SQLiteCommand account = new SQLiteCommand("SELECT * FROM accounts WHERE email = @email OR phone = @phone AND password = @password", DB);

            account.Parameters.AddWithValue("@email", this.textBoxMail.Text);
            account.Parameters.AddWithValue("@phone", this.textBoxNumber.Text);
            account.Parameters.AddWithValue("@password", this.textBoxPass.Text);

            SQLiteDataReader reader = null;

            reader = (SQLiteDataReader)await account.ExecuteReaderAsync();

            if (this.textBoxFirstName.Text == "" || this.textBoxLastName.Text == "" || this.textBoxMiddleName.Text == "" || this.textBoxCountry.Text == "" || this.textBoxCity.Text == "" || this.textBoxNumber.Text == "" || this.textBoxPass.Text == "" || this.textBoxMail.Text == "")
            {
                errorBox("Все поля должны быть заполнены");
            }
            else if (this.textBoxPass.Text != this.textBoxPassAgain.Text)
            {
                errorBox("Пароли не совпадают");
            }
            else if (await reader.ReadAsync())
            {
                errorBox("Учетная запись с таким телефоном или E-Mail существует");
            }
            else if (!EMail.IsValid(textBoxMail.Text) || !EMail.AllowedProvider(textBoxMail.Text)) {
                errorBox("Некорректный E-Mail\n\nРазрешенные e-mail сервисы для регистрации: " + string.Join(", ", EMail.allowedProviders));
            }
            else if (!PhoneNumber.IsValid(textBoxNumber.Text))
            {
                errorBox("Некорректный номер телефона: должен начинаться с 7 или 8 (без +) и содержать 11 цифр.");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("INSERT INTO accounts (firstName, lastName, middleName, country, city, phone, password, role, email) VALUES(@firstName, @lastName, @middleName, @country, @city, @phone, @password, @role, @email); ", DB);

                command.Parameters.AddWithValue("@firstName", this.textBoxFirstName.Text);
                command.Parameters.AddWithValue("@lastName", this.textBoxLastName.Text);
                command.Parameters.AddWithValue("@middleName", this.textBoxMiddleName.Text);
                command.Parameters.AddWithValue("@country", this.textBoxCountry.Text);
                command.Parameters.AddWithValue("@city", this.textBoxCity.Text);
                command.Parameters.AddWithValue("@phone", this.textBoxNumber.Text);
                command.Parameters.AddWithValue("@password", this.textBoxPass.Text);
                command.Parameters.AddWithValue("@role", "client");
                command.Parameters.AddWithValue("@email", this.textBoxMail.Text);

                await command.ExecuteNonQueryAsync();

                this.Hide();
                var signinForm = new Signin();
                signinForm.Show();
                signinForm.FormClosed += (s, args) => this.Show();
            }
        }


        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
