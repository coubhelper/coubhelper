﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Net.NetworkInformation;
using System.Net.Mail;

namespace techsupport
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainPage());
        }
    }

    // Строка с данными для соединения к базе данных
    public static class DataBase
    {
        public static string connection = @"Data Source=DataBase.db;Integrated Security=False; MultipleActiveResultSets=True";
    }

    #region Классы с данными таблиц SQLite
    // Структура таблицы аккаунтов
    public static class Accounts_table
    {
        public static string main = "accounts";
        public static string id = "id";
        public static string firstName = "firstName";
        public static string lastName = "lastName";
        public static string middleName = "middleName";
        public static string city = "city";
        public static string country = "country";
        public static string phone = "phone";
        public static string password = "password";
        public static string role = "role";
        public static string email = "email";
    }

    // Структура таблицы заявок
    public static class Tickets_table
    {
        public static string main = "tickets";
        public static string id = "id";
        public static string subject = "subject";
        public static string type = "type";
        public static string text = "text";
        public static string author = "author";
        public static string date = "date";
        public static string attachments = "attachments";
        public static string status = "status";
        public static string difficulty = "difficulty";
        public static string archieved = "archieved";
    }
    #endregion
    #region Валидация и конвертирование данных
    // Конвертирование имен из БД в человекочитаемые
    public static class NameResolver
    {
        public static string resolveTicketType(string type)
        {
            string resolved;

            switch (type)
            {
                case "problem":
                    resolved = "Проблема";
                    break;
                case "error":
                    resolved = "Ошибка";
                    break;
                case "question":
                    resolved = "Вопрос";
                    break;
                default:
                    resolved = "Проблема";
                    break;
            }

            return resolved;
        }

        public static string resolveTicketStatus(string status)
        {
            string resolved;

            switch (status)
            {
                case "inprogress":
                    resolved = "В процессе";
                    break;
                case "solved":
                    resolved = "Решена";
                    break;
                case "denied":
                    resolved = "Отклонена";
                    break;
                default:
                    resolved = "В процессе";
                    break;
            }

            return resolved;
        }
    }
    // Конвертирование из Unix времени в DateTime
    public static class UnixTime
    {
        public static DateTime ToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }
    }

    // Валидация E-Mail
    public static class EMail
    {
        public static bool IsValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string[] allowedProviders = { "gmail.com", "mail.ru", "yandex.ru", "list.ru", "bk.ru", "inbox.ru", "ya.ru" };

        public static bool AllowedProvider(string emailAddress)
        {
            try
            {
                string emailProvider = emailAddress.Split('@')[1];

                return allowedProviders.Contains(emailProvider);
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    // Валидация номера телефона
    public static class PhoneNumber
    {
        public static bool IsValid(string num)
        {
            try
            {
                long validNumber = Convert.ToInt64(num);
                // номера не менее 11 цифр, начинающиеся с 7 или 8
                return num.Length == 11 && (num[0] == '7' || num[0] == '8');
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    #endregion

    // Текущая пользовательская сессия
    public static class Session
    {
        public static string id = null;
        public static string firstName = null;
        public static string lastName = null;
        public static string middleName = null;
        public static string city = null;
        public static string country = null;
        public static string phone = null;
        public static string password = null;
        public static string role = null;
        public static string email = null;
    }
}
