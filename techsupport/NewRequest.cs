﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace techsupport
{
    public partial class NewRequest : Form
    {
        public NewRequest()
        {
            InitializeComponent();
        }

        private SQLiteConnection DB;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ComboBoxTicketType.SelectedItem = "Вопрос";
        }

        private async void NewRequest_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        private async void send_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("INSERT INTO main.tickets (subject, type, text, author, date, attachments, status, difficulty, archieved) VALUES (@subject, @type, @text, @author, @date, @attachments, @status, @difficulty, @archieved);", DB);
            command.Parameters.AddWithValue("@subject", this.TextBoxrequest_subject.Text);
            string reqType;
            switch (this.ComboBoxTicketType.SelectedItem) // конвертировать значение combobox'а 
            {
                case "Проблема":
                    reqType = "problem";
                    break;
                case "Ошибка":
                    reqType = "error";
                    break;
                case "Вопрос":
                    reqType = "question";
                    break;
                default:
                    reqType = "problem";
                    break;
            }

            if (TextBoxRequest.Text == "" || TextBoxrequest_subject.Text == "")
            {
                MessageBox.Show("Все поля должны быть заполнены.");
                return;
            }

            command.Parameters.AddWithValue("@type", reqType);
            command.Parameters.AddWithValue("@text", this.TextBoxRequest.Text);
            command.Parameters.AddWithValue("@author", Session.id);
            command.Parameters.AddWithValue("@date", DateTimeOffset.Now.ToUnixTimeSeconds());
            command.Parameters.AddWithValue("@attachments", "123123");
            command.Parameters.AddWithValue("@status", "inprogress");
            command.Parameters.AddWithValue("@difficulty", 1);
            command.Parameters.AddWithValue("@archieved", 0);

            await command.ExecuteNonQueryAsync();
            this.Close();
        }
    }
}
