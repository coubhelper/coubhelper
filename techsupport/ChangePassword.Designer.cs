﻿
namespace techsupport
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.labelChangePass = new System.Windows.Forms.Label();
            this.labelOldPass = new System.Windows.Forms.Label();
            this.textBoxOldPass = new System.Windows.Forms.TextBox();
            this.textBoxNewPass = new System.Windows.Forms.TextBox();
            this.labelNewPass = new System.Windows.Forms.Label();
            this.textBoxNewPassAgain = new System.Windows.Forms.TextBox();
            this.labelNewPassAgain = new System.Windows.Forms.Label();
            this.changePasswordSubmit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelChangePass
            // 
            this.labelChangePass.AutoSize = true;
            this.labelChangePass.BackColor = System.Drawing.Color.Black;
            this.labelChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelChangePass.ForeColor = System.Drawing.Color.White;
            this.labelChangePass.Location = new System.Drawing.Point(13, 13);
            this.labelChangePass.Name = "labelChangePass";
            this.labelChangePass.Size = new System.Drawing.Size(190, 25);
            this.labelChangePass.TabIndex = 0;
            this.labelChangePass.Text = "Сменить пароль";
            // 
            // labelOldPass
            // 
            this.labelOldPass.AutoSize = true;
            this.labelOldPass.BackColor = System.Drawing.Color.Black;
            this.labelOldPass.ForeColor = System.Drawing.Color.White;
            this.labelOldPass.Location = new System.Drawing.Point(15, 51);
            this.labelOldPass.Name = "labelOldPass";
            this.labelOldPass.Size = new System.Drawing.Size(84, 13);
            this.labelOldPass.TabIndex = 1;
            this.labelOldPass.Text = "Старый пароль";
            // 
            // textBoxOldPass
            // 
            this.textBoxOldPass.Location = new System.Drawing.Point(18, 68);
            this.textBoxOldPass.Name = "textBoxOldPass";
            this.textBoxOldPass.PasswordChar = '*';
            this.textBoxOldPass.Size = new System.Drawing.Size(185, 20);
            this.textBoxOldPass.TabIndex = 2;
            // 
            // textBoxNewPass
            // 
            this.textBoxNewPass.Location = new System.Drawing.Point(18, 112);
            this.textBoxNewPass.Name = "textBoxNewPass";
            this.textBoxNewPass.PasswordChar = '*';
            this.textBoxNewPass.Size = new System.Drawing.Size(185, 20);
            this.textBoxNewPass.TabIndex = 4;
            // 
            // labelNewPass
            // 
            this.labelNewPass.AutoSize = true;
            this.labelNewPass.BackColor = System.Drawing.Color.Black;
            this.labelNewPass.ForeColor = System.Drawing.Color.White;
            this.labelNewPass.Location = new System.Drawing.Point(15, 95);
            this.labelNewPass.Name = "labelNewPass";
            this.labelNewPass.Size = new System.Drawing.Size(80, 13);
            this.labelNewPass.TabIndex = 3;
            this.labelNewPass.Text = "Новый пароль";
            // 
            // textBoxNewPassAgain
            // 
            this.textBoxNewPassAgain.Location = new System.Drawing.Point(18, 153);
            this.textBoxNewPassAgain.Name = "textBoxNewPassAgain";
            this.textBoxNewPassAgain.PasswordChar = '*';
            this.textBoxNewPassAgain.Size = new System.Drawing.Size(185, 20);
            this.textBoxNewPassAgain.TabIndex = 6;
            // 
            // labelNewPassAgain
            // 
            this.labelNewPassAgain.AutoSize = true;
            this.labelNewPassAgain.BackColor = System.Drawing.Color.Black;
            this.labelNewPassAgain.ForeColor = System.Drawing.Color.White;
            this.labelNewPassAgain.Location = new System.Drawing.Point(15, 136);
            this.labelNewPassAgain.Name = "labelNewPassAgain";
            this.labelNewPassAgain.Size = new System.Drawing.Size(165, 13);
            this.labelNewPassAgain.TabIndex = 5;
            this.labelNewPassAgain.Text = "Подтверждение нового пароля";
            // 
            // changePasswordSubmit
            // 
            this.changePasswordSubmit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.changePasswordSubmit.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.changePasswordSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.changePasswordSubmit.ForeColor = System.Drawing.Color.White;
            this.changePasswordSubmit.Location = new System.Drawing.Point(18, 180);
            this.changePasswordSubmit.Name = "changePasswordSubmit";
            this.changePasswordSubmit.Size = new System.Drawing.Size(101, 23);
            this.changePasswordSubmit.TabIndex = 7;
            this.changePasswordSubmit.Text = "Сменить пароль";
            this.changePasswordSubmit.UseVisualStyleBackColor = false;
            this.changePasswordSubmit.Click += new System.EventHandler(this.changePasswordSubmit_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(254, 231);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(238, 231);
            this.Controls.Add(this.changePasswordSubmit);
            this.Controls.Add(this.textBoxNewPassAgain);
            this.Controls.Add(this.labelNewPassAgain);
            this.Controls.Add(this.textBoxNewPass);
            this.Controls.Add(this.labelNewPass);
            this.Controls.Add(this.textBoxOldPass);
            this.Controls.Add(this.labelOldPass);
            this.Controls.Add(this.labelChangePass);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ChangePassword";
            this.Text = "Сменить пароль";
            this.Load += new System.EventHandler(this.ChangePassword_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelChangePass;
        private System.Windows.Forms.Label labelOldPass;
        private System.Windows.Forms.TextBox textBoxOldPass;
        private System.Windows.Forms.TextBox textBoxNewPass;
        private System.Windows.Forms.Label labelNewPass;
        private System.Windows.Forms.TextBox textBoxNewPassAgain;
        private System.Windows.Forms.Label labelNewPassAgain;
        private System.Windows.Forms.Button changePasswordSubmit;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}