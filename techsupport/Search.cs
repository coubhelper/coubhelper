﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace techsupport
{
    public partial class Search : Form
    {
        SQLiteConnection DB;

        public Search()
        {
            InitializeComponent();
        }

        private async void searchSubmit_Click(object sender, EventArgs e)
        {
            dataGridView_Tickets.Rows.Clear();
            SQLiteDataReader sqlReader = null;
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM [{Tickets_table.main}] WHERE subject LIKE @search", DB);

            string search = "%" + textBoxFind.Text + "%"; // Паттерн для поиска - любое вхождение подстроки в тему заявки

            command.Parameters.AddWithValue("@search", search);

            List<string[]> data = new List<string[]>();
            try
            {
                sqlReader = (SQLiteDataReader)await command.ExecuteReaderAsync();
                while (await sqlReader.ReadAsync())
                {
                    if (Session.role == "client" && sqlReader["author"].ToString() != Session.id)
                    {
                        continue; // не показывать клиентам чужие заявки
                    }

                    string ticketType, ticketAuthor, ticketDate, ticketStatus;

                    // Человекочитаемые тип и статус заявки
                    ticketType = NameResolver.resolveTicketType(Convert.ToString(sqlReader["type"]));
                    ticketStatus = NameResolver.resolveTicketStatus(Convert.ToString(sqlReader["status"]));

                    // ФИО автора вместо id
                    SQLiteCommand authorCmd = new SQLiteCommand("SELECT firstName, lastName, middleName from accounts WHERE id = @id", DB);
                    authorCmd.Parameters.AddWithValue("@id", sqlReader["author"]);
                    SQLiteDataReader authorReader = (SQLiteDataReader)await authorCmd.ExecuteReaderAsync();
                    await authorReader.ReadAsync();
                    ticketAuthor = authorReader["lastName"] + " " + authorReader["firstName"] + " " + authorReader["middleName"];

                    // Время вместо unix time
                    DateTime ticketTimestamp = UnixTime.ToDateTime(Convert.ToDouble(sqlReader["date"]));
                    ticketDate = $"{ticketTimestamp.Day}.{ticketTimestamp.Month}.{ticketTimestamp.Year}, {ticketTimestamp.Hour}:{ticketTimestamp.Minute}";

                    data.Add(new string[6]);

                    data[data.Count - 1][0] = Convert.ToString(sqlReader["id"]);
                    data[data.Count - 1][1] = Convert.ToString(sqlReader["subject"]);
                    data[data.Count - 1][2] = ticketType;
                    data[data.Count - 1][3] = ticketAuthor;
                    data[data.Count - 1][4] = ticketDate;
                    data[data.Count - 1][5] = ticketStatus;
                }

                foreach (string[] s in data)
                {
                    dataGridView_Tickets.Rows.Add(s);
                }
                dataGridView_Tickets.ClearSelection();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }

            }
        }

        private async void Search_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        public List<Form> ticketForms = new List<Form>();
        private void dataGridView_Tickets_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                var row = (DataGridViewRow)senderGrid.Rows[e.RowIndex];

                var ticket = new Ticket();
                ticket.currentId = row.Cells[0].Value.ToString(); // передаем в новую форму ID заявки
                ticket.Show();
                ticketForms.Add(ticket);
            }
        }

        private void Search_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form ticket in ticketForms)
            {
                ticket.Close();
            }
        }
    }
}
