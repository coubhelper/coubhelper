﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace techsupport
{
    public partial class Adminsettings : Form
    {
        public Adminsettings()
        {
            InitializeComponent();
        }

        #region открытие новых форм

        private Form addStaff = new AddStaff();
        private Form staffBase = new StaffBase();
        private Form clientsBase = new ClientsBase();

        private void button_AddStaff_Click(object sender, EventArgs e)
        {
            if (addStaff.IsDisposed)
                addStaff = new AddStaff();
            addStaff.Show();
            addStaff.Focus();

        }
        private void button_Staff_Click(object sender, EventArgs e)
        {
            if (staffBase.IsDisposed)
                staffBase = new StaffBase();
            staffBase.Show();
            staffBase.Focus();

        }

        private void button_Clients_Click(object sender, EventArgs e)
        {
            if (clientsBase.IsDisposed)
                clientsBase = new ClientsBase();
            clientsBase.Show();
            clientsBase.Focus();
        }

        #endregion

        private void Adminsettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            addStaff.Close();
            staffBase.Close();
            clientsBase.Close();
        }
    }
}
