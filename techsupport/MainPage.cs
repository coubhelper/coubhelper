﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace techsupport
{
    public partial class MainPage : Form
    {
        public MainPage()
        {
            InitializeComponent();
        }
        #region открытие новых форм
        private void openSignup_Click(object sender, EventArgs e)
        {
            this.Hide();
            var signup = new Signup();
            signup.FormClosed += (s, args) => this.Show(); // открывать главную форму после закрытия регистрации
            signup.Show();
            
        }

        private void openSignin_Click(object sender, EventArgs e)
        {
            this.Hide();
            var signin = new Signin();
            signin.FormClosed += (s, args) => this.Show(); // открывать главную форму после закрытия входа
            signin.Show();
        }

        #endregion

        #region Очистка журнала БД

        private SQLiteConnection DB;
        private async void MainPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Удаление журнала БД при выходе
            SQLiteCommand command = new SQLiteCommand("PRAGMA wal_checkpoint(TRUNCATE)", DB);
            await command.ExecuteNonQueryAsync();
        }

        private void MainPage_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            DB.OpenAsync();
        }

        #endregion
    }
}
