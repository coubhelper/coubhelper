﻿
namespace techsupport
{
    partial class Adminsettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Adminsettings));
            this.button_Staff = new System.Windows.Forms.Button();
            this.button_Clients = new System.Windows.Forms.Button();
            this.button_AddStaff = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Staff
            // 
            this.button_Staff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_Staff.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_Staff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Staff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Staff.ForeColor = System.Drawing.Color.White;
            this.button_Staff.Location = new System.Drawing.Point(54, 12);
            this.button_Staff.Name = "button_Staff";
            this.button_Staff.Size = new System.Drawing.Size(156, 35);
            this.button_Staff.TabIndex = 0;
            this.button_Staff.Text = "Сотрудники";
            this.button_Staff.UseVisualStyleBackColor = false;
            this.button_Staff.Click += new System.EventHandler(this.button_Staff_Click);
            // 
            // button_Clients
            // 
            this.button_Clients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_Clients.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_Clients.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Clients.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Clients.ForeColor = System.Drawing.Color.White;
            this.button_Clients.Location = new System.Drawing.Point(347, 12);
            this.button_Clients.Name = "button_Clients";
            this.button_Clients.Size = new System.Drawing.Size(156, 35);
            this.button_Clients.TabIndex = 1;
            this.button_Clients.Text = "Клиенты";
            this.button_Clients.UseVisualStyleBackColor = false;
            this.button_Clients.Click += new System.EventHandler(this.button_Clients_Click);
            // 
            // button_AddStaff
            // 
            this.button_AddStaff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_AddStaff.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.button_AddStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AddStaff.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_AddStaff.ForeColor = System.Drawing.Color.White;
            this.button_AddStaff.Location = new System.Drawing.Point(13, 82);
            this.button_AddStaff.Name = "button_AddStaff";
            this.button_AddStaff.Size = new System.Drawing.Size(248, 40);
            this.button_AddStaff.TabIndex = 2;
            this.button_AddStaff.Text = "Добавить сотрудника";
            this.button_AddStaff.UseVisualStyleBackColor = false;
            this.button_AddStaff.Click += new System.EventHandler(this.button_AddStaff_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(715, 453);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // Adminsettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(713, 450);
            this.Controls.Add(this.button_AddStaff);
            this.Controls.Add(this.button_Clients);
            this.Controls.Add(this.button_Staff);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Adminsettings";
            this.Text = "Администрирование";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Adminsettings_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Staff;
        private System.Windows.Forms.Button button_Clients;
        private System.Windows.Forms.Button button_AddStaff;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}