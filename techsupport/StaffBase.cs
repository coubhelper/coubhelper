﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace techsupport
{
    public partial class StaffBase : Form
    {
        private SQLiteConnection DB;
        public StaffBase()
        {
            InitializeComponent();
        }


        private async void StaffBase_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
            LoadingClients();
        }
        private async void LoadingClients()
        {
            dataGridView_BaseStaff.Rows.Clear();
            SQLiteDataReader sqlReader = null;
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM [{Accounts_table.main}] WHERE role = 'staff'", DB);
            List<string[]> data = new List<string[]>();
            try
            {
                sqlReader = (SQLiteDataReader)await command.ExecuteReaderAsync();
                while (await sqlReader.ReadAsync())
                {
                    data.Add(new string[10]);

                    data[data.Count - 1][0] = Convert.ToString($"{sqlReader[$"{Accounts_table.id}"]}");
                    data[data.Count - 1][1] = Convert.ToString($"{sqlReader[$"{Accounts_table.lastName}"]}");
                    data[data.Count - 1][2] = Convert.ToString($"{sqlReader[$"{Accounts_table.firstName}"]}");
                    data[data.Count - 1][3] = Convert.ToString($"{sqlReader[$"{Accounts_table.middleName}"]}");
                    data[data.Count - 1][5] = Convert.ToString($"{sqlReader[$"{Accounts_table.city}"]}");
                    data[data.Count - 1][4] = Convert.ToString($"{sqlReader[$"{Accounts_table.country}"]}");
                    data[data.Count - 1][7] = Convert.ToString($"{sqlReader[$"{Accounts_table.phone}"]}");
                    data[data.Count - 1][6] = Convert.ToString($"{sqlReader[$"{Accounts_table.email}"]}");
                }

                foreach (string[] s in data)
                {
                    dataGridView_BaseStaff.Rows.Add(s);
                }
                dataGridView_BaseStaff.ClearSelection();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }
        }

        public List<Form> clientForms = new List<Form>();

        private void dataGridView_BaseStaff_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                var senderGrid = (DataGridView)sender;
                var row = senderGrid.Rows[e.RowIndex];

                var clientAdminOpts = new ClientAdminOpts(row.Cells[0].Value.ToString());
                clientAdminOpts.Show();
                clientForms.Add(clientAdminOpts);
            }
        }

        private void StaffBase_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (Form clientAdminOpts in clientForms)
            {
                clientAdminOpts.Close();
            }
        }
    }
}
