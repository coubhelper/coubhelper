﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace techsupport
{
    public partial class Ticket : Form
    {
        public Ticket()
        {
            InitializeComponent();
        }
        private async void closeTicket_Click(object sender, EventArgs e)
        {
            this.sendQuestion.Hide();
            this.closeTicket.Enabled = false;

            SQLiteCommand command = new SQLiteCommand("UPDATE tickets SET status = @status WHERE id = @id", DB);
            command.Parameters.AddWithValue("@status", "solved");
            command.Parameters.AddWithValue("@id", this.currentId);

            try
            {
                await command.ExecuteNonQueryAsync();
                MessageBox.Show("Заявка закрыта");
                this.textBoxAnswer.ReadOnly = true;
                this.textBoxMessage.ReadOnly = true;
                this.sendQuestion.Hide();
                this.saveReply.Hide();
                saveTicket.Hide();
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        public string currentId;
        private SQLiteConnection DB;

        public async void refreshReplyAuthor(int replyAuthor)
        {
            SQLiteCommand replyAuthorCmd = new SQLiteCommand("SELECT * FROM accounts WHERE id = @id", DB);
            replyAuthorCmd.Parameters.AddWithValue("@id", replyAuthor);
            var replyAuthorReader = (SQLiteDataReader)await replyAuthorCmd.ExecuteReaderAsync();

            try
            {
                if (await replyAuthorReader.ReadAsync())
                {
                    this.labelReplyAuthor.Text = replyAuthorReader["lastName"].ToString() + " " + replyAuthorReader["firstName"].ToString() + " " + replyAuthorReader["middleName"].ToString();
                }
                else
                {
                    MessageBox.Show("Сотрудник, ответивший на заявку, был удален");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void Ticket_Load(object sender, EventArgs e)
        {
            if (Session.role == "client") // Не показывать клиенту интерфейс сотрудников
            {
                this.saveReply.Hide();
                this.saveTicket.Hide();
                declineButton.Hide();
                buttonRemove.Hide();
            }
            else if (Session.role == "staff")
            {
                this.sendQuestion.Hide();
                this.labelQuest.Hide();
                this.closeTicket.Hide();
                saveTicket.Hide();
                buttonRemove.Hide();

                this.textBoxAnswer.ReadOnly = false;
            }
            else
            {
                sendQuestion.Show();
                labelQuest.Show();
                closeTicket.Show();

                textBoxAnswer.ReadOnly = false;
                textBoxMessage.ReadOnly = false;
            }

            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();

            SQLiteCommand command = new SQLiteCommand("SELECT * FROM tickets WHERE id = @id", DB);
            command.Parameters.AddWithValue("@id", this.currentId);

            SQLiteDataReader reader = null;
            
            try
            {
                reader = (SQLiteDataReader)await command.ExecuteReaderAsync();
                if (await reader.ReadAsync())
                {
                    this.textBoxSubject.Text = reader["subject"].ToString();
                    this.textBoxMessage.Text = reader["text"].ToString();
                    this.textBoxAnswer.Text = reader["reply"].ToString();

                    if (reader["status"].ToString() == "solved" || reader["status"].ToString() == "denied")
                    {
                        this.textBoxAnswer.ReadOnly = true;
                        this.textBoxMessage.ReadOnly = true;
                        this.sendQuestion.Hide();
                        this.saveReply.Hide();
                        this.closeTicket.Hide();
                        this.labelQuest.Hide();
                        declineButton.Hide();

                        if (reader["status"].ToString() == "denied")
                        {
                            this.labelDeclined.Show();
                        }
                    }

                    SQLiteCommand authorCmd = new SQLiteCommand("SELECT * FROM accounts WHERE id = @id", DB);

                    authorCmd.Parameters.AddWithValue("@id", reader["author"]);

                    var authorReader = (SQLiteDataReader)await authorCmd.ExecuteReaderAsync();
                    
                    try
                    {
                        if (await authorReader.ReadAsync())
                        {
                            this.labelName.Text = authorReader["lastName"].ToString() +" "+ authorReader["firstName"].ToString() +" "+ authorReader["middleName"].ToString();
                        }
                        else
                        {
                            MessageBox.Show("Пользователь, создавший эту заявку, удален");
                            this.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    refreshReplyAuthor(Convert.ToInt32(reader["replyLastEditor"].ToString()));
                } else
                {
                    MessageBox.Show("Заявка была удалена");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                // если ответа на заявку нет будет вызван FormatException, его показывать не надо
                if (ex.GetType().Name != "FormatException")
                    MessageBox.Show(ex.Message);
                else if (Session.role == "client") // ответа нет => нельзя закрыть вопрос
                {
                    this.labelReplyAuthor.Hide();
                    this.textBoxAnswer.Hide();
                    this.labelQuest.Hide();
                    this.closeTicket.Hide();

                    // перемещаем кнопку отправки
                    this.sendQuestion.Location = new Point(21, 262);
                }
            }
        }

        private void sendQuestion_Click(object sender, EventArgs e)
        {
            this.textBoxMessage.ReadOnly = false;
            this.saveTicket.Show();
        }

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Заявка",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        #region кнопки сохранения
        private async void saveReply_Click(object sender, EventArgs e)
        {
            if (textBoxAnswer.Text == "")
            {
                errorBox("Ответ на заявку не может быть пустым");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("UPDATE tickets SET reply = @reply, replyLastEditor = @replyLastEditor WHERE id = @id", DB);
                command.Parameters.AddWithValue("@id", this.currentId);
                command.Parameters.AddWithValue("@reply", textBoxAnswer.Text);
                command.Parameters.AddWithValue("@replyLastEditor", Session.id);

                await command.ExecuteNonQueryAsync();

                int id = Convert.ToInt32(Session.id);

                refreshReplyAuthor(id);

                MessageBox.Show("Изменения внесены успешно");
            }
        }

        private async void saveTicket_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text == "")
            {
                errorBox("Заявка не может быть пустой");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("UPDATE tickets SET text = @message WHERE id = @id", DB);
                command.Parameters.AddWithValue("@message", this.textBoxMessage.Text);
                command.Parameters.AddWithValue("@id", this.currentId);

                await command.ExecuteNonQueryAsync();

                MessageBox.Show("Изменения внесены успешно");
                saveTicket.Hide();
                textBoxMessage.ReadOnly = true;
;            }
        }

        #endregion

        private async void declineButton_Click(object sender, EventArgs e)
        {
            this.sendQuestion.Hide();
            this.closeTicket.Enabled = false;

            SQLiteCommand command = new SQLiteCommand("UPDATE tickets SET status = @status WHERE id = @id", DB);
            command.Parameters.AddWithValue("@status", "denied");
            command.Parameters.AddWithValue("@id", this.currentId);

            try
            {
                await command.ExecuteNonQueryAsync();
                MessageBox.Show("Заявка отклонена");
                declineButton.Hide();
                this.textBoxAnswer.ReadOnly = true;
                this.textBoxMessage.ReadOnly = true;
                this.sendQuestion.Hide();
                this.saveReply.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private async void buttonRemove_Click(object sender, EventArgs e)
        {
            SQLiteCommand cmd = new SQLiteCommand("DELETE FROM tickets WHERE id = @id", DB);
            cmd.Parameters.AddWithValue("@id", currentId);
            await cmd.ExecuteNonQueryAsync();

            MessageBox.Show("Заявка удалена");
            this.Close();
        }
    }
}
