﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Security.Policy;

namespace techsupport
{
    public partial class AddStaff : Form
    {
        public AddStaff()
        {
            InitializeComponent();
        }

        private SQLiteConnection DB;

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Добавить сотрудника",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private async void button_AddStaff_Click(object sender, EventArgs e)
        {
            SQLiteCommand account = new SQLiteCommand("SELECT * FROM accounts WHERE email = @email OR phone = @phone AND password = @password", DB);

            account.Parameters.AddWithValue("@email", this.textBoxMail.Text);
            account.Parameters.AddWithValue("@phone", this.textBoxPhone.Text);
            account.Parameters.AddWithValue("@password", this.textBoxPass.Text);

            SQLiteDataReader reader = null;

            reader = (SQLiteDataReader)await account.ExecuteReaderAsync();

            if (await reader.ReadAsync())
            {
                errorBox("Пользователь с таким номером или E-Mail уже существует");
            }
            else if (this.textBoxPass.Text != this.textBoxPassAgain.Text)
            {
                errorBox("Пароли не совпадают");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("INSERT INTO accounts (firstName, lastName, middleName, country, city, phone, password, role, email) VALUES(@firstName, @lastName, @middleName, @country, @city, @phone, @password, @role, @email); ", DB);

                string firstName, lastName, middleName;
                string[] nameSplitted;

                nameSplitted = textBoxFIO.Text.Split(' '); // Разделить ФИО
                try
                {
                    firstName = nameSplitted[0];
                    lastName = nameSplitted[1];
                    middleName = nameSplitted[2];

                    command.Parameters.AddWithValue("@firstName", firstName);
                    command.Parameters.AddWithValue("@lastName", lastName);
                    command.Parameters.AddWithValue("@middleName", middleName);
                    command.Parameters.AddWithValue("@country", this.textBoxCountry.Text);
                    command.Parameters.AddWithValue("@city", this.textBoxCity.Text);
                    command.Parameters.AddWithValue("@phone", this.textBoxPhone.Text);
                    command.Parameters.AddWithValue("@password", this.textBoxPass.Text);
                    command.Parameters.AddWithValue("@role", "staff");
                    command.Parameters.AddWithValue("@email", this.textBoxMail.Text);

                    await command.ExecuteNonQueryAsync();

                    this.Hide();
                }
                catch (Exception ex)
                {
                    errorBox("Нужно полное ФИО");
                }
            }
        }

        private async void AddStaff_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }
    }
}
