﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Runtime.InteropServices;

namespace techsupport
{
    public partial class tickets : Form
    {
        private SQLiteConnection DB;
        public tickets()
        {
            InitializeComponent();
            profile = new Profile();
            search = new Search();
            newRequest = new NewRequest();
            adminSettings = new Adminsettings();
        }

        #region Открытие новых форм

        private Form profile;
        private Form search;
        private Form newRequest;
        private Form adminSettings;
        private void openProfile_Click(object sender, EventArgs e)
        {
            if (profile.IsDisposed)
                profile = new Profile();
            profile.Show();
            profile.Focus();
        }

        private void search_Click(object sender, EventArgs e)
        {
            if (search.IsDisposed)
                search = new Search();
            search.Show();
            search.Focus();
        }

        private void newTicket_Click(object sender, EventArgs e)
        {
            if (newRequest.IsDisposed)
                newRequest = new NewRequest();
            newRequest.Show();
            newRequest.Focus();
        }

        private void adminSettingsButton_Click(object sender, EventArgs e)
        {
            if (adminSettings.IsDisposed)
                adminSettings = new Adminsettings();
            adminSettings.Show();
            adminSettings.Focus();
        }

        #endregion

        #region Загрузка заявок
        
        private void refreshName()
        {
            this.labelName.Text = Session.lastName + " " + Session.firstName + " " + Session.middleName;
        }

        private async void tickets_Load(object sender, EventArgs e)
        {
            this.ComboBoxType.SelectedItem = "Вопрос";
            this.ComboBoxStatus.SelectedItem = "В процессе";

            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
            LoadingClients();

            refreshName();

            if (Session.role != "admin")
            {
                this.ButtonAdminSettings.Visible = false;
            }

            if (Session.role != "staff" && Session.role != "admin")
            {
                this.buttonExcelDump.Visible = false;
            }
        }

        private async void LoadingClients()
        {
            dataGridView_Tickets.Rows.Clear();
            SQLiteDataReader sqlReader = null;
            SQLiteCommand command = new SQLiteCommand($"SELECT * FROM [{Tickets_table.main}] WHERE type = @type AND status = @status", DB);

            string type, status;

            switch (this.ComboBoxType.Text)
            {
                case "Проблема":
                    type = "problem";
                    break;
                case "Ошибка":
                    type = "error";
                    break;
                case "Вопрос":
                    type = "question";
                    break;
                default:
                    type = "problem";
                    break;
            }

            switch (this.ComboBoxStatus.Text)
            {
                case "В процессе":
                    status = "inprogress";
                    break;
                case "Решена":
                    status = "solved";
                    break;
                case "Отклонена":
                    status = "denied";
                    break;
                default:
                    status = "inprogress";
                    break;
            }

            command.Parameters.AddWithValue("@type", type);
            command.Parameters.AddWithValue("@status", status);

            List<string[]> data = new List<string[]>();
            try
            {
                sqlReader = (SQLiteDataReader)await command.ExecuteReaderAsync();
                while (await sqlReader.ReadAsync())
                {
                    if (Session.role == "client" && sqlReader["author"].ToString() != Session.id)
                    {
                        continue; // не показывать клиентам чужие заявки
                    }

                    string ticketType, ticketAuthor, ticketDate, ticketStatus;

                    // человекочитаемые статус и тип заявки
                    ticketType = NameResolver.resolveTicketType(Convert.ToString(sqlReader["type"]));
                    ticketStatus = NameResolver.resolveTicketStatus(Convert.ToString(sqlReader["status"]));

                    // ФИО автора вместо id
                    try
                    {
                        SQLiteCommand authorCmd = new SQLiteCommand("SELECT firstName, lastName, middleName from accounts WHERE id = @id", DB);
                        authorCmd.Parameters.AddWithValue("@id", sqlReader["author"]);
                        SQLiteDataReader authorReader = (SQLiteDataReader)await authorCmd.ExecuteReaderAsync();
                        await authorReader.ReadAsync();
                        ticketAuthor = authorReader["lastName"] + " " + authorReader["firstName"] + " " + authorReader["middleName"];
                    } catch (Exception)
                    {
                        continue; // Не отображаем заявку, если ее автор был удалён
                    }

                    // Время вместо unix time
                    DateTime ticketTimestamp = UnixTime.ToDateTime(Convert.ToDouble(sqlReader["date"]));
                    ticketDate = ticketTimestamp.ToString("dd.MM.yyyy, HH:mm:ss");

                    data.Add(new string[6]);

                    data[data.Count - 1][0] = Convert.ToString(sqlReader["id"]);
                    data[data.Count - 1][1] = Convert.ToString(sqlReader["subject"]);
                    data[data.Count - 1][2] = ticketType;
                    data[data.Count - 1][3] = ticketAuthor;
                    data[data.Count - 1][4] = ticketDate;
                    data[data.Count - 1][5] = ticketStatus;
                }

                foreach (string[] s in data)
                {
                    dataGridView_Tickets.Rows.Add(s);
                }
                dataGridView_Tickets.ClearSelection();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), ex.Source.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
                
            }
        }

        public List<Form> ticketForms = new List<Form>();
        private void dataGridView_Tickets_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1) {
                var senderGrid = (DataGridView)sender;
                var row = (DataGridViewRow)senderGrid.Rows[e.RowIndex];

                var ticket = new Ticket();
                ticket.currentId = row.Cells[0].Value.ToString();
                ticket.Show();
                ticketForms.Add(ticket);
            }
        }

        #endregion

        #region Обновление списка заявок
        private void ButtonRefresh_Click(object sender, EventArgs e)
        {
            LoadingClients();
            refreshName();
        }

        private void ComboBoxStatus_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadingClients();
        }

        private void ComboBoxType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            LoadingClients();
        }
        #endregion

        private void tickets_FormClosing(object sender, FormClosingEventArgs e)
        {
            profile.Close();
            newRequest.Close();
            adminSettings.Close();
            search.Close();

            foreach (Form ticket in ticketForms)
            {
                ticket.Close();
            }
        }

        private async void buttonExcelDump_Click(object sender, EventArgs e)
        {
            DateTime date = DateTime.Now;
            string dateStr = date.ToString("dd.MM.yyyy_HH.mm.ss");

            using (var writer = new SwiftExcel.ExcelWriter($".\\{dateStr}.xlsx"))
            {
                writer.Write("ID", 1, 1);
                writer.Write("Тема заявки", 2, 1);
                writer.Write("Тип", 3, 1);
                writer.Write("Автор", 4, 1);
                writer.Write("Дата", 5, 1);
                writer.Write("Статус заявки", 6, 1);

                SQLiteCommand command = new SQLiteCommand("SELECT * FROM tickets WHERE status = @status", DB);
                command.Parameters.AddWithValue("@status", "solved");

                SQLiteDataReader reader = (SQLiteDataReader) await command.ExecuteReaderAsync();

                int i = 2; // начинаем со 2 строки, т.к. 1 - шапка таблицы
                while (await reader.ReadAsync())
                {
                    string ticketType, ticketAuthor, ticketDate, ticketStatus;

                    // человекочитаемые статус и тип заявки
                    ticketType = NameResolver.resolveTicketType(Convert.ToString(reader["type"]));
                    ticketStatus = NameResolver.resolveTicketStatus(Convert.ToString(reader["status"]));

                    // ФИО автора вместо id
                    SQLiteCommand authorCmd = new SQLiteCommand("SELECT firstName, lastName, middleName from accounts WHERE id = @id", DB);
                    authorCmd.Parameters.AddWithValue("@id", reader["author"]);
                    SQLiteDataReader authorReader = (SQLiteDataReader)await authorCmd.ExecuteReaderAsync();
                    await authorReader.ReadAsync();
                    ticketAuthor = authorReader["lastName"] + " " + authorReader["firstName"] + " " + authorReader["middleName"];

                    // Время вместо unix time
                    DateTime ticketTimestamp = UnixTime.ToDateTime(Convert.ToDouble(reader["date"]));
                    ticketDate = ticketTimestamp.ToString("dd.MM.yyyy, HH:mm:ss");

                    writer.Write(Convert.ToString(reader["id"]), 1, i);
                    writer.Write(Convert.ToString(reader["subject"]), 2, i);
                    writer.Write(ticketType, 3, i);
                    writer.Write(ticketAuthor, 4, i);
                    writer.Write(ticketDate, 5, i);
                    writer.Write(ticketStatus, 6, i);

                    i++;
                }
            }

            MessageBox.Show("Созданный отчет находится в " + dateStr + ".xlsx");
        }

        private void buttonLogout_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
