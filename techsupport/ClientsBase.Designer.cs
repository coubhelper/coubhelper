﻿
namespace techsupport
{
    partial class ClientsBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientsBase));
            this.label_ClientsBase = new System.Windows.Forms.Label();
            this.dataGridView_ClientsBase = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ColumnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMiddleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCountry = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ClientsBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label_ClientsBase
            // 
            this.label_ClientsBase.AutoSize = true;
            this.label_ClientsBase.BackColor = System.Drawing.Color.Black;
            this.label_ClientsBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_ClientsBase.ForeColor = System.Drawing.Color.White;
            this.label_ClientsBase.Location = new System.Drawing.Point(12, 9);
            this.label_ClientsBase.Name = "label_ClientsBase";
            this.label_ClientsBase.Size = new System.Drawing.Size(99, 25);
            this.label_ClientsBase.TabIndex = 0;
            this.label_ClientsBase.Text = "Клиенты";
            // 
            // dataGridView_ClientsBase
            // 
            this.dataGridView_ClientsBase.AllowUserToAddRows = false;
            this.dataGridView_ClientsBase.AllowUserToDeleteRows = false;
            this.dataGridView_ClientsBase.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_ClientsBase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ClientsBase.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnID,
            this.ColumnLastName,
            this.ColumnFirstName,
            this.ColumnMiddleName,
            this.ColumnCity,
            this.ColumnCountry,
            this.ColumnNumber,
            this.ColumnMail});
            this.dataGridView_ClientsBase.Location = new System.Drawing.Point(17, 54);
            this.dataGridView_ClientsBase.Name = "dataGridView_ClientsBase";
            this.dataGridView_ClientsBase.ReadOnly = true;
            this.dataGridView_ClientsBase.RowHeadersVisible = false;
            this.dataGridView_ClientsBase.Size = new System.Drawing.Size(724, 324);
            this.dataGridView_ClientsBase.TabIndex = 1;
            this.dataGridView_ClientsBase.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_ClientsBase_CellDoubleClick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 404);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // ColumnID
            // 
            this.ColumnID.HeaderText = "ID";
            this.ColumnID.Name = "ColumnID";
            this.ColumnID.ReadOnly = true;
            this.ColumnID.Width = 50;
            // 
            // ColumnLastName
            // 
            this.ColumnLastName.HeaderText = "Фамилия";
            this.ColumnLastName.Name = "ColumnLastName";
            this.ColumnLastName.ReadOnly = true;
            this.ColumnLastName.Width = 90;
            // 
            // ColumnFirstName
            // 
            this.ColumnFirstName.HeaderText = "Имя";
            this.ColumnFirstName.Name = "ColumnFirstName";
            this.ColumnFirstName.ReadOnly = true;
            this.ColumnFirstName.Width = 70;
            // 
            // ColumnMiddleName
            // 
            this.ColumnMiddleName.HeaderText = "Отчество";
            this.ColumnMiddleName.Name = "ColumnMiddleName";
            this.ColumnMiddleName.ReadOnly = true;
            this.ColumnMiddleName.Width = 110;
            // 
            // ColumnCity
            // 
            this.ColumnCity.HeaderText = "Город";
            this.ColumnCity.Name = "ColumnCity";
            this.ColumnCity.ReadOnly = true;
            this.ColumnCity.Width = 60;
            // 
            // ColumnCountry
            // 
            this.ColumnCountry.HeaderText = "Страна";
            this.ColumnCountry.Name = "ColumnCountry";
            this.ColumnCountry.ReadOnly = true;
            this.ColumnCountry.Width = 60;
            // 
            // ColumnNumber
            // 
            this.ColumnNumber.HeaderText = "Номер телефона";
            this.ColumnNumber.Name = "ColumnNumber";
            this.ColumnNumber.ReadOnly = true;
            this.ColumnNumber.Width = 120;
            // 
            // ColumnMail
            // 
            this.ColumnMail.HeaderText = "E-Mail";
            this.ColumnMail.Name = "ColumnMail";
            this.ColumnMail.ReadOnly = true;
            this.ColumnMail.Width = 155;
            // 
            // ClientsBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 393);
            this.Controls.Add(this.dataGridView_ClientsBase);
            this.Controls.Add(this.label_ClientsBase);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ClientsBase";
            this.Text = "Администрирование: Клиенты";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientsBase_FormClosing);
            this.Load += new System.EventHandler(this.ClientsBase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ClientsBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_ClientsBase;
        private System.Windows.Forms.DataGridView dataGridView_ClientsBase;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMiddleName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCountry;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMail;
    }
}