﻿
namespace techsupport
{
    partial class Signin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Signin));
            this.textBoxNumberOrMail = new System.Windows.Forms.TextBox();
            this.labelNumberOrMail = new System.Windows.Forms.Label();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.labelPass = new System.Windows.Forms.Label();
            this.ButtonSignIN = new System.Windows.Forms.Button();
            this.labelSignIN = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxNumberOrMail
            // 
            this.textBoxNumberOrMail.Location = new System.Drawing.Point(11, 64);
            this.textBoxNumberOrMail.Name = "textBoxNumberOrMail";
            this.textBoxNumberOrMail.Size = new System.Drawing.Size(100, 20);
            this.textBoxNumberOrMail.TabIndex = 0;
            // 
            // labelNumberOrMail
            // 
            this.labelNumberOrMail.AutoSize = true;
            this.labelNumberOrMail.BackColor = System.Drawing.Color.Black;
            this.labelNumberOrMail.ForeColor = System.Drawing.Color.White;
            this.labelNumberOrMail.Location = new System.Drawing.Point(8, 48);
            this.labelNumberOrMail.Name = "labelNumberOrMail";
            this.labelNumberOrMail.Size = new System.Drawing.Size(146, 13);
            this.labelNumberOrMail.TabIndex = 1;
            this.labelNumberOrMail.Text = "Номер телефона или E-Mail";
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(12, 105);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.PasswordChar = '*';
            this.textBoxPass.Size = new System.Drawing.Size(100, 20);
            this.textBoxPass.TabIndex = 0;
            // 
            // labelPass
            // 
            this.labelPass.AutoSize = true;
            this.labelPass.BackColor = System.Drawing.Color.Black;
            this.labelPass.ForeColor = System.Drawing.Color.White;
            this.labelPass.Location = new System.Drawing.Point(9, 89);
            this.labelPass.Name = "labelPass";
            this.labelPass.Size = new System.Drawing.Size(45, 13);
            this.labelPass.TabIndex = 1;
            this.labelPass.Text = "Пароль";
            // 
            // ButtonSignIN
            // 
            this.ButtonSignIN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonSignIN.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonSignIN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSignIN.ForeColor = System.Drawing.Color.White;
            this.ButtonSignIN.Location = new System.Drawing.Point(11, 131);
            this.ButtonSignIN.Name = "ButtonSignIN";
            this.ButtonSignIN.Size = new System.Drawing.Size(75, 23);
            this.ButtonSignIN.TabIndex = 3;
            this.ButtonSignIN.Text = "Вход";
            this.ButtonSignIN.UseVisualStyleBackColor = false;
            this.ButtonSignIN.Click += new System.EventHandler(this.signinSubmit_Click);
            // 
            // labelSignIN
            // 
            this.labelSignIN.AutoSize = true;
            this.labelSignIN.BackColor = System.Drawing.Color.Black;
            this.labelSignIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSignIN.ForeColor = System.Drawing.Color.White;
            this.labelSignIN.Location = new System.Drawing.Point(8, 9);
            this.labelSignIN.Name = "labelSignIN";
            this.labelSignIN.Size = new System.Drawing.Size(65, 25);
            this.labelSignIN.TabIndex = 1;
            this.labelSignIN.Text = "Вход";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(198, 224);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonBack.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.ForeColor = System.Drawing.Color.White;
            this.buttonBack.Location = new System.Drawing.Point(92, 131);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 5;
            this.buttonBack.Text = "Назад";
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // Signin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(197, 203);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.ButtonSignIN);
            this.Controls.Add(this.labelPass);
            this.Controls.Add(this.labelSignIN);
            this.Controls.Add(this.labelNumberOrMail);
            this.Controls.Add(this.textBoxPass);
            this.Controls.Add(this.textBoxNumberOrMail);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Signin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Вход";
            this.Load += new System.EventHandler(this.Signin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxNumberOrMail;
        private System.Windows.Forms.Label labelNumberOrMail;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.Label labelPass;
        private System.Windows.Forms.Button ButtonSignIN;
        private System.Windows.Forms.Label labelSignIN;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button buttonBack;
    }
}