﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace techsupport
{
    public partial class Signin : Form
    {
        public Signin()
        {
            InitializeComponent();
        }

        private SQLiteConnection DB;

        private async void Signin_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Вход",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private async void signinSubmit_Click(object sender, EventArgs e)
        {
            SQLiteCommand command = new SQLiteCommand("SELECT * FROM accounts WHERE email = @login OR phone = @login AND password = @password", DB);

            command.Parameters.AddWithValue("@login", this.textBoxNumberOrMail.Text);
            command.Parameters.AddWithValue("@password", this.textBoxPass.Text);

            SQLiteDataReader reader = null;

            try
            {
                reader = (SQLiteDataReader)await command.ExecuteReaderAsync();
                if (await reader.ReadAsync())
                {
                    // создаем сессию
                    Session.id = Convert.ToString(reader["id"]);
                    Session.city = Convert.ToString(reader["city"]);
                    Session.country = Convert.ToString(reader["country"]);
                    Session.email = Convert.ToString(reader["email"]);
                    Session.firstName = Convert.ToString(reader["firstName"]);
                    Session.lastName = Convert.ToString(reader["lastName"]);
                    Session.middleName = Convert.ToString(reader["middleName"]);
                    Session.password = Convert.ToString(reader["password"]);
                    Session.phone = Convert.ToString(reader["phone"]);
                    Session.role = Convert.ToString(reader["role"]);

                    this.Hide();
                    var ticketsForm = new tickets();
                    ticketsForm.Show();
                    ticketsForm.FormClosed += (s, args) => this.Show();
                }
                else
                {
                    errorBox("Неправильные логин или пароль");
                }
            }
            catch (Exception ex)
            {
                errorBox(ex.ToString());
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
