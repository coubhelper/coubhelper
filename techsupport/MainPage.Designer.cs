﻿
namespace techsupport
{
    partial class MainPage
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPage));
            this.label_main = new System.Windows.Forms.Label();
            this.openSignup = new System.Windows.Forms.Button();
            this.openSignin = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // label_main
            // 
            this.label_main.AutoSize = true;
            this.label_main.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label_main.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_main.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label_main.Location = new System.Drawing.Point(13, 13);
            this.label_main.Name = "label_main";
            this.label_main.Size = new System.Drawing.Size(314, 25);
            this.label_main.TabIndex = 0;
            this.label_main.Text = "Учет заявок пользователей";
            // 
            // openSignup
            // 
            this.openSignup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.openSignup.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.openSignup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openSignup.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.openSignup.ForeColor = System.Drawing.SystemColors.Control;
            this.openSignup.Location = new System.Drawing.Point(44, 208);
            this.openSignup.Margin = new System.Windows.Forms.Padding(0);
            this.openSignup.Name = "openSignup";
            this.openSignup.Size = new System.Drawing.Size(192, 35);
            this.openSignup.TabIndex = 1;
            this.openSignup.Text = "Регистрация";
            this.openSignup.UseVisualStyleBackColor = false;
            this.openSignup.Click += new System.EventHandler(this.openSignup_Click);
            // 
            // openSignin
            // 
            this.openSignin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.openSignin.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.openSignin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.openSignin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.openSignin.ForeColor = System.Drawing.SystemColors.Control;
            this.openSignin.Location = new System.Drawing.Point(301, 208);
            this.openSignin.Name = "openSignin";
            this.openSignin.Size = new System.Drawing.Size(192, 35);
            this.openSignin.TabIndex = 1;
            this.openSignin.Text = "Вход";
            this.openSignin.UseVisualStyleBackColor = false;
            this.openSignin.Click += new System.EventHandler(this.openSignin_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(-2, -6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(593, 467);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // MainPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 461);
            this.Controls.Add(this.openSignin);
            this.Controls.Add(this.openSignup);
            this.Controls.Add(this.label_main);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Учет заявок пользователей";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPage_FormClosing);
            this.Load += new System.EventHandler(this.MainPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_main;
        private System.Windows.Forms.Button openSignup;
        private System.Windows.Forms.Button openSignin;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

