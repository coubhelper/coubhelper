﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace techsupport
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private SQLiteConnection DB;
        private async void ChangePassword_Load(object sender, EventArgs e)
        {
            DB = new SQLiteConnection(DataBase.connection);
            await DB.OpenAsync();
        }

        private void errorBox(string message)
        {
            MessageBox.Show(
                message,
                "Смена пароля",
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private async void changePasswordSubmit_Click(object sender, EventArgs e)
        {
            if (this.textBoxOldPass.Text != Session.password)
            {
                errorBox("Неправильно введен старый пароль");
            }
            else if (this.textBoxNewPass.Text != this.textBoxNewPassAgain.Text)
            {
                errorBox("Новый пароль и подтверждение должны совпадать");
            }
            else
            {
                SQLiteCommand command = new SQLiteCommand("UPDATE accounts SET password = @password WHERE id = @id", DB);
                command.Parameters.AddWithValue("@password", this.textBoxNewPass.Text);
                command.Parameters.AddWithValue("@id", Session.id);

                await command.ExecuteNonQueryAsync();

                Session.password = this.textBoxNewPass.Text;

                MessageBox.Show(
                    "Пароль изменен",
                    "Смена пароля",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );

                this.Close();
            }
        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
