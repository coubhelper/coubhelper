﻿
namespace techsupport
{
    partial class tickets
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(tickets));
            this.ButtonLK = new System.Windows.Forms.Button();
            this.labelName = new System.Windows.Forms.Label();
            this.labelRequest = new System.Windows.Forms.Label();
            this.ComboBoxType = new System.Windows.Forms.ComboBox();
            this.ComboBoxStatus = new System.Windows.Forms.ComboBox();
            this.ButtonNewTicket = new System.Windows.Forms.Button();
            this.ButtonFind = new System.Windows.Forms.Button();
            this.labelType = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.ButtonAdminSettings = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridView_Tickets = new System.Windows.Forms.DataGridView();
            this.ColumnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSubject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAuthor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonRefresh = new System.Windows.Forms.Button();
            this.buttonExcelDump = new System.Windows.Forms.Button();
            this.buttonLogout = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Tickets)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonLK
            // 
            this.ButtonLK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonLK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonLK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLK.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonLK.ForeColor = System.Drawing.Color.White;
            this.ButtonLK.Location = new System.Drawing.Point(17, 359);
            this.ButtonLK.Name = "ButtonLK";
            this.ButtonLK.Size = new System.Drawing.Size(132, 33);
            this.ButtonLK.TabIndex = 0;
            this.ButtonLK.Text = "Личный кабинет";
            this.ButtonLK.UseVisualStyleBackColor = false;
            this.ButtonLK.Click += new System.EventHandler(this.openProfile_Click);
            // 
            // labelName
            // 
            this.labelName.BackColor = System.Drawing.Color.Black;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(0, 0);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(166, 106);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Заболотный Юрий Иванович";
            // 
            // labelRequest
            // 
            this.labelRequest.AutoSize = true;
            this.labelRequest.BackColor = System.Drawing.Color.Black;
            this.labelRequest.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelRequest.ForeColor = System.Drawing.Color.White;
            this.labelRequest.Location = new System.Drawing.Point(155, 13);
            this.labelRequest.Name = "labelRequest";
            this.labelRequest.Size = new System.Drawing.Size(88, 25);
            this.labelRequest.TabIndex = 3;
            this.labelRequest.Text = "Заявки";
            // 
            // ComboBoxType
            // 
            this.ComboBoxType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxType.FormattingEnabled = true;
            this.ComboBoxType.Items.AddRange(new object[] {
            "Проблема",
            "Ошибка",
            "Вопрос"});
            this.ComboBoxType.Location = new System.Drawing.Point(228, 44);
            this.ComboBoxType.Name = "ComboBoxType";
            this.ComboBoxType.Size = new System.Drawing.Size(96, 21);
            this.ComboBoxType.TabIndex = 4;
            this.ComboBoxType.SelectionChangeCommitted += new System.EventHandler(this.ComboBoxType_SelectionChangeCommitted);
            // 
            // ComboBoxStatus
            // 
            this.ComboBoxStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxStatus.FormattingEnabled = true;
            this.ComboBoxStatus.Items.AddRange(new object[] {
            "В процессе",
            "Решена",
            "Отклонена"});
            this.ComboBoxStatus.Location = new System.Drawing.Point(416, 44);
            this.ComboBoxStatus.Name = "ComboBoxStatus";
            this.ComboBoxStatus.Size = new System.Drawing.Size(96, 21);
            this.ComboBoxStatus.TabIndex = 4;
            this.ComboBoxStatus.SelectionChangeCommitted += new System.EventHandler(this.ComboBoxStatus_SelectionChangeCommitted);
            // 
            // ButtonNewTicket
            // 
            this.ButtonNewTicket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonNewTicket.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonNewTicket.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonNewTicket.ForeColor = System.Drawing.Color.White;
            this.ButtonNewTicket.Location = new System.Drawing.Point(595, 42);
            this.ButtonNewTicket.Name = "ButtonNewTicket";
            this.ButtonNewTicket.Size = new System.Drawing.Size(100, 22);
            this.ButtonNewTicket.TabIndex = 5;
            this.ButtonNewTicket.Text = "Новая заявка";
            this.ButtonNewTicket.UseVisualStyleBackColor = false;
            this.ButtonNewTicket.Click += new System.EventHandler(this.newTicket_Click);
            // 
            // ButtonFind
            // 
            this.ButtonFind.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonFind.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonFind.ForeColor = System.Drawing.Color.White;
            this.ButtonFind.Location = new System.Drawing.Point(750, 43);
            this.ButtonFind.Name = "ButtonFind";
            this.ButtonFind.Size = new System.Drawing.Size(87, 22);
            this.ButtonFind.TabIndex = 6;
            this.ButtonFind.Text = "Найти";
            this.ButtonFind.UseVisualStyleBackColor = false;
            this.ButtonFind.Click += new System.EventHandler(this.search_Click);
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.BackColor = System.Drawing.Color.Black;
            this.labelType.ForeColor = System.Drawing.Color.White;
            this.labelType.Location = new System.Drawing.Point(157, 46);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(65, 13);
            this.labelType.TabIndex = 9;
            this.labelType.Text = "Тип заявки";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.BackColor = System.Drawing.Color.Black;
            this.labelStatus.ForeColor = System.Drawing.Color.White;
            this.labelStatus.Location = new System.Drawing.Point(330, 47);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(80, 13);
            this.labelStatus.TabIndex = 10;
            this.labelStatus.Text = "Статус заявки";
            // 
            // ButtonAdminSettings
            // 
            this.ButtonAdminSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonAdminSettings.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonAdminSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonAdminSettings.ForeColor = System.Drawing.Color.White;
            this.ButtonAdminSettings.Location = new System.Drawing.Point(17, 294);
            this.ButtonAdminSettings.Name = "ButtonAdminSettings";
            this.ButtonAdminSettings.Size = new System.Drawing.Size(132, 59);
            this.ButtonAdminSettings.TabIndex = 12;
            this.ButtonAdminSettings.Text = "Настройки админа";
            this.ButtonAdminSettings.UseVisualStyleBackColor = false;
            this.ButtonAdminSettings.Click += new System.EventHandler(this.adminSettingsButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-4, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(869, 501);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridView_Tickets
            // 
            this.dataGridView_Tickets.AllowUserToAddRows = false;
            this.dataGridView_Tickets.AllowUserToDeleteRows = false;
            this.dataGridView_Tickets.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dataGridView_Tickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Tickets.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnID,
            this.ColumnSubject,
            this.ColumnType,
            this.ColumnAuthor,
            this.ColumnDate,
            this.ColumnStatus});
            this.dataGridView_Tickets.Location = new System.Drawing.Point(160, 72);
            this.dataGridView_Tickets.Name = "dataGridView_Tickets";
            this.dataGridView_Tickets.ReadOnly = true;
            this.dataGridView_Tickets.RowHeadersVisible = false;
            this.dataGridView_Tickets.Size = new System.Drawing.Size(687, 366);
            this.dataGridView_Tickets.TabIndex = 14;
            this.dataGridView_Tickets.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Tickets_CellDoubleClick);
            // 
            // ColumnID
            // 
            this.ColumnID.HeaderText = "ID";
            this.ColumnID.Name = "ColumnID";
            this.ColumnID.ReadOnly = true;
            this.ColumnID.Width = 30;
            // 
            // ColumnSubject
            // 
            this.ColumnSubject.HeaderText = "Тема заявки";
            this.ColumnSubject.Name = "ColumnSubject";
            this.ColumnSubject.ReadOnly = true;
            // 
            // ColumnType
            // 
            this.ColumnType.HeaderText = "Тип";
            this.ColumnType.Name = "ColumnType";
            this.ColumnType.ReadOnly = true;
            // 
            // ColumnAuthor
            // 
            this.ColumnAuthor.HeaderText = "Автор";
            this.ColumnAuthor.Name = "ColumnAuthor";
            this.ColumnAuthor.ReadOnly = true;
            this.ColumnAuthor.Width = 250;
            // 
            // ColumnDate
            // 
            this.ColumnDate.HeaderText = "Дата";
            this.ColumnDate.Name = "ColumnDate";
            this.ColumnDate.ReadOnly = true;
            // 
            // ColumnStatus
            // 
            this.ColumnStatus.HeaderText = "Статус";
            this.ColumnStatus.Name = "ColumnStatus";
            this.ColumnStatus.ReadOnly = true;
            // 
            // ButtonRefresh
            // 
            this.ButtonRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonRefresh.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonRefresh.ForeColor = System.Drawing.Color.White;
            this.ButtonRefresh.Location = new System.Drawing.Point(518, 42);
            this.ButtonRefresh.Name = "ButtonRefresh";
            this.ButtonRefresh.Size = new System.Drawing.Size(71, 22);
            this.ButtonRefresh.TabIndex = 5;
            this.ButtonRefresh.Text = "Обновить";
            this.ButtonRefresh.UseVisualStyleBackColor = false;
            this.ButtonRefresh.Click += new System.EventHandler(this.ButtonRefresh_Click);
            // 
            // buttonExcelDump
            // 
            this.buttonExcelDump.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonExcelDump.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonExcelDump.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExcelDump.ForeColor = System.Drawing.Color.White;
            this.buttonExcelDump.Location = new System.Drawing.Point(160, 444);
            this.buttonExcelDump.Name = "buttonExcelDump";
            this.buttonExcelDump.Size = new System.Drawing.Size(172, 22);
            this.buttonExcelDump.TabIndex = 15;
            this.buttonExcelDump.Text = "Отчет по закрытым заявкам";
            this.buttonExcelDump.UseVisualStyleBackColor = false;
            this.buttonExcelDump.Click += new System.EventHandler(this.buttonExcelDump_Click);
            // 
            // buttonLogout
            // 
            this.buttonLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonLogout.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogout.ForeColor = System.Drawing.Color.White;
            this.buttonLogout.Location = new System.Drawing.Point(17, 398);
            this.buttonLogout.Name = "buttonLogout";
            this.buttonLogout.Size = new System.Drawing.Size(132, 40);
            this.buttonLogout.TabIndex = 16;
            this.buttonLogout.Text = "Выйти";
            this.buttonLogout.UseVisualStyleBackColor = false;
            this.buttonLogout.Click += new System.EventHandler(this.buttonLogout_Click);
            // 
            // tickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 471);
            this.Controls.Add(this.buttonLogout);
            this.Controls.Add(this.buttonExcelDump);
            this.Controls.Add(this.dataGridView_Tickets);
            this.Controls.Add(this.ButtonAdminSettings);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.ButtonFind);
            this.Controls.Add(this.ButtonRefresh);
            this.Controls.Add(this.ButtonNewTicket);
            this.Controls.Add(this.ComboBoxStatus);
            this.Controls.Add(this.ComboBoxType);
            this.Controls.Add(this.labelRequest);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.ButtonLK);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "tickets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Главная";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.tickets_FormClosing);
            this.Load += new System.EventHandler(this.tickets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Tickets)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonLK;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelRequest;
        private System.Windows.Forms.ComboBox ComboBoxType;
        private System.Windows.Forms.ComboBox ComboBoxStatus;
        private System.Windows.Forms.Button ButtonNewTicket;
        private System.Windows.Forms.Button ButtonFind;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button ButtonAdminSettings;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dataGridView_Tickets;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSubject;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAuthor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnStatus;
        private System.Windows.Forms.Button ButtonRefresh;
        private System.Windows.Forms.Button buttonExcelDump;
        private System.Windows.Forms.Button buttonLogout;
    }
}

