﻿
namespace techsupport
{
    partial class NewRequest
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewRequest));
            this.TextBoxrequest_subject = new System.Windows.Forms.TextBox();
            this.Text_Subject_request = new System.Windows.Forms.Label();
            this.ComboBoxTicketType = new System.Windows.Forms.ComboBox();
            this.TextBoxRequest = new System.Windows.Forms.TextBox();
            this.ButtonSend = new System.Windows.Forms.Button();
            this.Text_New_request = new System.Windows.Forms.Label();
            this.attachment = new System.Windows.Forms.OpenFileDialog();
            this.labelType = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // TextBoxrequest_subject
            // 
            this.TextBoxrequest_subject.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBoxrequest_subject.Location = new System.Drawing.Point(12, 76);
            this.TextBoxrequest_subject.Multiline = true;
            this.TextBoxrequest_subject.Name = "TextBoxrequest_subject";
            this.TextBoxrequest_subject.Size = new System.Drawing.Size(757, 48);
            this.TextBoxrequest_subject.TabIndex = 0;
            // 
            // Text_Subject_request
            // 
            this.Text_Subject_request.AutoSize = true;
            this.Text_Subject_request.BackColor = System.Drawing.Color.Black;
            this.Text_Subject_request.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text_Subject_request.ForeColor = System.Drawing.Color.White;
            this.Text_Subject_request.Location = new System.Drawing.Point(13, 57);
            this.Text_Subject_request.Name = "Text_Subject_request";
            this.Text_Subject_request.Size = new System.Drawing.Size(96, 18);
            this.Text_Subject_request.TabIndex = 1;
            this.Text_Subject_request.Text = "Тема заявки";
            // 
            // ComboBoxTicketType
            // 
            this.ComboBoxTicketType.BackColor = System.Drawing.SystemColors.Control;
            this.ComboBoxTicketType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTicketType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBoxTicketType.FormattingEnabled = true;
            this.ComboBoxTicketType.Items.AddRange(new object[] {
            "Проблема",
            "Ошибка",
            "Вопрос"});
            this.ComboBoxTicketType.Location = new System.Drawing.Point(106, 130);
            this.ComboBoxTicketType.Name = "ComboBoxTicketType";
            this.ComboBoxTicketType.Size = new System.Drawing.Size(102, 26);
            this.ComboBoxTicketType.TabIndex = 2;
            // 
            // TextBoxRequest
            // 
            this.TextBoxRequest.Location = new System.Drawing.Point(12, 163);
            this.TextBoxRequest.Multiline = true;
            this.TextBoxRequest.Name = "TextBoxRequest";
            this.TextBoxRequest.Size = new System.Drawing.Size(757, 170);
            this.TextBoxRequest.TabIndex = 3;
            // 
            // ButtonSend
            // 
            this.ButtonSend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonSend.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.ButtonSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSend.ForeColor = System.Drawing.Color.White;
            this.ButtonSend.Location = new System.Drawing.Point(12, 352);
            this.ButtonSend.Name = "ButtonSend";
            this.ButtonSend.Size = new System.Drawing.Size(122, 33);
            this.ButtonSend.TabIndex = 4;
            this.ButtonSend.Text = "Отправить";
            this.ButtonSend.UseVisualStyleBackColor = false;
            this.ButtonSend.Click += new System.EventHandler(this.send_Click);
            // 
            // Text_New_request
            // 
            this.Text_New_request.AutoSize = true;
            this.Text_New_request.BackColor = System.Drawing.Color.Black;
            this.Text_New_request.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text_New_request.ForeColor = System.Drawing.Color.White;
            this.Text_New_request.Location = new System.Drawing.Point(7, 9);
            this.Text_New_request.Name = "Text_New_request";
            this.Text_New_request.Size = new System.Drawing.Size(147, 25);
            this.Text_New_request.TabIndex = 8;
            this.Text_New_request.Text = "Новая заявка";
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.BackColor = System.Drawing.Color.Black;
            this.labelType.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelType.ForeColor = System.Drawing.Color.White;
            this.labelType.Location = new System.Drawing.Point(9, 133);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(85, 18);
            this.labelType.TabIndex = 1;
            this.labelType.Text = "Тип заявки";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(797, 403);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // NewRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 399);
            this.Controls.Add(this.Text_New_request);
            this.Controls.Add(this.ButtonSend);
            this.Controls.Add(this.TextBoxRequest);
            this.Controls.Add(this.ComboBoxTicketType);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.Text_Subject_request);
            this.Controls.Add(this.TextBoxrequest_subject);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "NewRequest";
            this.Text = "Создать заявку";
            this.Load += new System.EventHandler(this.NewRequest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxrequest_subject;
        private System.Windows.Forms.Label Text_Subject_request;
        private System.Windows.Forms.ComboBox ComboBoxTicketType;
        private System.Windows.Forms.TextBox TextBoxRequest;
        private System.Windows.Forms.Button ButtonSend;
        private System.Windows.Forms.Label Text_New_request;
        private System.Windows.Forms.OpenFileDialog attachment;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

