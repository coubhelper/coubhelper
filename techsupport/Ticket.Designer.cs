﻿namespace techsupport
{
    partial class Ticket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ticket));
            this.labelTicket = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelType = new System.Windows.Forms.Label();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.labelReplyAuthor = new System.Windows.Forms.Label();
            this.textBoxAnswer = new System.Windows.Forms.TextBox();
            this.labelQuest = new System.Windows.Forms.Label();
            this.closeTicket = new System.Windows.Forms.Button();
            this.sendQuestion = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxSubject = new System.Windows.Forms.TextBox();
            this.saveReply = new System.Windows.Forms.Button();
            this.saveTicket = new System.Windows.Forms.Button();
            this.declineButton = new System.Windows.Forms.Button();
            this.labelDeclined = new System.Windows.Forms.Label();
            this.buttonRemove = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTicket
            // 
            this.labelTicket.AutoSize = true;
            this.labelTicket.BackColor = System.Drawing.Color.Black;
            this.labelTicket.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTicket.ForeColor = System.Drawing.Color.White;
            this.labelTicket.Location = new System.Drawing.Point(8, 9);
            this.labelTicket.Name = "labelTicket";
            this.labelTicket.Size = new System.Drawing.Size(95, 25);
            this.labelTicket.TabIndex = 0;
            this.labelTicket.Text = "Заявка ";
            // 
            // labelName
            // 
            this.labelName.BackColor = System.Drawing.Color.Black;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(380, -2);
            this.labelName.Name = "labelName";
            this.labelName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelName.Size = new System.Drawing.Size(308, 70);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Заболотный Юрий Иваныч";
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.BackColor = System.Drawing.Color.Black;
            this.labelType.ForeColor = System.Drawing.Color.White;
            this.labelType.Location = new System.Drawing.Point(18, 55);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(73, 13);
            this.labelType.TabIndex = 1;
            this.labelType.Text = "Тема заявки";
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(18, 103);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.ReadOnly = true;
            this.textBoxMessage.Size = new System.Drawing.Size(670, 154);
            this.textBoxMessage.TabIndex = 2;
            // 
            // labelReplyAuthor
            // 
            this.labelReplyAuthor.AutoSize = true;
            this.labelReplyAuthor.BackColor = System.Drawing.Color.Black;
            this.labelReplyAuthor.ForeColor = System.Drawing.Color.White;
            this.labelReplyAuthor.Location = new System.Drawing.Point(18, 288);
            this.labelReplyAuthor.Name = "labelReplyAuthor";
            this.labelReplyAuthor.Size = new System.Drawing.Size(90, 13);
            this.labelReplyAuthor.TabIndex = 1;
            this.labelReplyAuthor.Text = "Ответ на заявку";
            // 
            // textBoxAnswer
            // 
            this.textBoxAnswer.Location = new System.Drawing.Point(18, 304);
            this.textBoxAnswer.Multiline = true;
            this.textBoxAnswer.Name = "textBoxAnswer";
            this.textBoxAnswer.ReadOnly = true;
            this.textBoxAnswer.Size = new System.Drawing.Size(670, 154);
            this.textBoxAnswer.TabIndex = 2;
            // 
            // labelQuest
            // 
            this.labelQuest.AutoSize = true;
            this.labelQuest.BackColor = System.Drawing.Color.Black;
            this.labelQuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelQuest.ForeColor = System.Drawing.Color.White;
            this.labelQuest.Location = new System.Drawing.Point(13, 467);
            this.labelQuest.Name = "labelQuest";
            this.labelQuest.Size = new System.Drawing.Size(187, 20);
            this.labelQuest.TabIndex = 3;
            this.labelQuest.Text = "Вам помог этот ответ?";
            // 
            // closeTicket
            // 
            this.closeTicket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.closeTicket.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.closeTicket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.closeTicket.ForeColor = System.Drawing.Color.White;
            this.closeTicket.Location = new System.Drawing.Point(21, 490);
            this.closeTicket.Name = "closeTicket";
            this.closeTicket.Size = new System.Drawing.Size(125, 23);
            this.closeTicket.TabIndex = 4;
            this.closeTicket.Text = "Да, закрыть заявку";
            this.closeTicket.UseVisualStyleBackColor = false;
            this.closeTicket.Click += new System.EventHandler(this.closeTicket_Click);
            // 
            // sendQuestion
            // 
            this.sendQuestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.sendQuestion.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.sendQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.sendQuestion.ForeColor = System.Drawing.Color.White;
            this.sendQuestion.Location = new System.Drawing.Point(152, 490);
            this.sendQuestion.Name = "sendQuestion";
            this.sendQuestion.Size = new System.Drawing.Size(130, 23);
            this.sendQuestion.TabIndex = 4;
            this.sendQuestion.Text = "Редактировать заявку";
            this.sendQuestion.UseVisualStyleBackColor = false;
            this.sendQuestion.Click += new System.EventHandler(this.sendQuestion_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-1, -3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(702, 534);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxSubject
            // 
            this.textBoxSubject.Location = new System.Drawing.Point(18, 71);
            this.textBoxSubject.Name = "textBoxSubject";
            this.textBoxSubject.ReadOnly = true;
            this.textBoxSubject.Size = new System.Drawing.Size(670, 20);
            this.textBoxSubject.TabIndex = 6;
            this.textBoxSubject.Text = "lorem it";
            // 
            // saveReply
            // 
            this.saveReply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.saveReply.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.saveReply.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.saveReply.ForeColor = System.Drawing.Color.White;
            this.saveReply.Location = new System.Drawing.Point(588, 464);
            this.saveReply.Name = "saveReply";
            this.saveReply.Size = new System.Drawing.Size(100, 23);
            this.saveReply.TabIndex = 7;
            this.saveReply.Text = "Сохранить ответ";
            this.saveReply.UseVisualStyleBackColor = false;
            this.saveReply.Click += new System.EventHandler(this.saveReply_Click);
            // 
            // saveTicket
            // 
            this.saveTicket.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.saveTicket.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.saveTicket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.saveTicket.ForeColor = System.Drawing.Color.White;
            this.saveTicket.Location = new System.Drawing.Point(575, 263);
            this.saveTicket.Name = "saveTicket";
            this.saveTicket.Size = new System.Drawing.Size(108, 23);
            this.saveTicket.TabIndex = 8;
            this.saveTicket.Text = "Сохранить заявку";
            this.saveTicket.UseVisualStyleBackColor = false;
            this.saveTicket.Click += new System.EventHandler(this.saveTicket_Click);
            // 
            // declineButton
            // 
            this.declineButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.declineButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.declineButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.declineButton.ForeColor = System.Drawing.Color.White;
            this.declineButton.Location = new System.Drawing.Point(513, 464);
            this.declineButton.Name = "declineButton";
            this.declineButton.Size = new System.Drawing.Size(69, 23);
            this.declineButton.TabIndex = 9;
            this.declineButton.Text = "Отклонить заявку";
            this.declineButton.UseVisualStyleBackColor = false;
            this.declineButton.Click += new System.EventHandler(this.declineButton_Click);
            // 
            // labelDeclined
            // 
            this.labelDeclined.AutoSize = true;
            this.labelDeclined.BackColor = System.Drawing.Color.Black;
            this.labelDeclined.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDeclined.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.labelDeclined.Location = new System.Drawing.Point(242, 467);
            this.labelDeclined.Name = "labelDeclined";
            this.labelDeclined.Size = new System.Drawing.Size(192, 20);
            this.labelDeclined.TabIndex = 10;
            this.labelDeclined.Text = "Заявка была отклонена";
            this.labelDeclined.Visible = false;
            // 
            // buttonRemove
            // 
            this.buttonRemove.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonRemove.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(41)))), ((int)(((byte)(41)))));
            this.buttonRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRemove.ForeColor = System.Drawing.Color.White;
            this.buttonRemove.Location = new System.Drawing.Point(513, 490);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(69, 23);
            this.buttonRemove.TabIndex = 11;
            this.buttonRemove.Text = "Удалить";
            this.buttonRemove.UseVisualStyleBackColor = false;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // Ticket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 529);
            this.Controls.Add(this.buttonRemove);
            this.Controls.Add(this.labelDeclined);
            this.Controls.Add(this.declineButton);
            this.Controls.Add(this.saveTicket);
            this.Controls.Add(this.saveReply);
            this.Controls.Add(this.textBoxSubject);
            this.Controls.Add(this.sendQuestion);
            this.Controls.Add(this.closeTicket);
            this.Controls.Add(this.labelQuest);
            this.Controls.Add(this.textBoxAnswer);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.labelReplyAuthor);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.labelTicket);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Ticket";
            this.Text = "Просмотр заявки";
            this.Load += new System.EventHandler(this.Ticket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTicket;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelType;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label labelReplyAuthor;
        private System.Windows.Forms.TextBox textBoxAnswer;
        private System.Windows.Forms.Label labelQuest;
        private System.Windows.Forms.Button closeTicket;
        private System.Windows.Forms.Button sendQuestion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxSubject;
        private System.Windows.Forms.Button saveReply;
        private System.Windows.Forms.Button saveTicket;
        private System.Windows.Forms.Button declineButton;
        private System.Windows.Forms.Label labelDeclined;
        private System.Windows.Forms.Button buttonRemove;
    }
}